package com.puntodamar.spring.soap.demo.service;

import com.puntodamar.spring.soap.demo.bean.Course;
import com.puntodamar.spring_soap.CourseDetail;
import com.puntodamar.spring_soap.Status;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class CourseDetailService {
    private static List<Course> courses = new ArrayList<Course>();
    public enum Status {
        SUCCESS, FAILED
    }

    static {
        courses.add(new Course(1, "Spring Boot", "Spring Boot Description"));
        courses.add(new Course(2, "Node", "Node Description"));
    }

    public Course findById(int id){
        for(Course course : courses){
            if(course.getId() == id)
                return course;
        }
        return null;
    }

    public List<Course> all(){
        return courses;
    }

    public Status deleteById(int id) {
        Iterator<Course> iterator =  courses.iterator();
        while(iterator.hasNext()){
            Course course = iterator.next();
            if(course.getId() == id){
                iterator.remove();
                return Status.SUCCESS;
            }
        }
        return Status.FAILED;
    }

    public CourseDetail map(Course course){
        CourseDetail detail = new CourseDetail();
        detail.setId(course.getId());
        detail.setName(course.getName());
        detail.setDescription(course.getDescription());
        return detail;
    }

    public List<CourseDetail> map(List<Course> courses){
        List<CourseDetail> mapped = new ArrayList<CourseDetail>();
        for(Course course : courses){
            mapped.add(map(course));
        }
        return mapped;
    }

    public com.puntodamar.spring_soap.Status mapStatus(Status status){
        return status == Status.FAILED ? com.puntodamar.spring_soap.Status.FAILED : com.puntodamar.spring_soap.Status.SUCCESS;
    }
}

package com.puntodamar.spring.soap.demo.course;

import com.puntodamar.spring.soap.demo.bean.Course;
import com.puntodamar.spring.soap.demo.service.CourseDetailService;
import com.puntodamar.spring_soap.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class DeleteCourseDetailEndpoint {

    @Autowired
    CourseDetailService service;

    @PayloadRoot(namespace = "http://puntodamar.com/spring-soap", localPart ="DeleteCourseDetailRequest")
    @ResponsePayload
    public DeleteCourseDetailResponse get(@RequestPayload DeleteCourseDetailRequest request){
        DeleteCourseDetailResponse response = new DeleteCourseDetailResponse();
        response.setStatus(service.mapStatus(service.deleteById(request.getId())));
        return response;
    }
}

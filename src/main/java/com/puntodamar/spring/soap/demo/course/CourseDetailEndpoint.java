package com.puntodamar.spring.soap.demo.course;

import com.puntodamar.spring.soap.demo.bean.Course;
import com.puntodamar.spring.soap.demo.service.CourseDetailService;
import com.puntodamar.spring_soap.CourseDetail;
import com.puntodamar.spring_soap.GetCourseDetailRequest;
import com.puntodamar.spring_soap.GetCourseDetailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CourseDetailEndpoint {

    @Autowired
    CourseDetailService service;

    @PayloadRoot(namespace = "http://puntodamar.com/spring-soap", localPart ="GetCourseDetailRequest")
    @ResponsePayload
    public GetCourseDetailResponse get(@RequestPayload GetCourseDetailRequest request){
        Course course = service.findById(request.getId());
        CourseDetail mapped = service.map(course);
        GetCourseDetailResponse response = new GetCourseDetailResponse();
        response.setCourseDetail(mapped);
        return response;
    }
}

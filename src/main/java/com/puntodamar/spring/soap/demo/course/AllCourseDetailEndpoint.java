package com.puntodamar.spring.soap.demo.course;

import com.puntodamar.spring.soap.demo.bean.Course;
import com.puntodamar.spring.soap.demo.service.CourseDetailService;
import com.puntodamar.spring_soap.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class AllCourseDetailEndpoint {

    @Autowired
    CourseDetailService service;

    @PayloadRoot(namespace = "http://puntodamar.com/spring-soap", localPart ="GetAllCourseDetailRequest")
    @ResponsePayload
    public GetAllCourseDetailResponse get(@RequestPayload GetAllCourseDetailRequest request){
        GetAllCourseDetailResponse response = new GetAllCourseDetailResponse();
        List<Course> courses = service.all();
        response.getCourseDetail().addAll(service.map(courses));
        return response;
    }
}
